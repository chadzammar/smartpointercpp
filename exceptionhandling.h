/*
 * exceptionhandling.h
 *
 *  Created on:
 *  Author: Chad
 */

#ifndef EXCEPTIONHANDLING_H_
#define EXCEPTIONHANDLING_H_

#include <iostream>
using namespace std;

// Negative number exception
struct NegativeNumberException : public exception {
	const char* what() const throw(){
		return "Negative number exception ...";
	}
};

// Out of range exception
struct RangeException : public exception {
	const char* what() const throw() {
		return "Out of range exception ...";
	}
};

// Illegal array size exception
struct IllegalArraySizeException : public exception {
	const char* what () const throw (){
		return "Illegal value for array size.";
	}
};

#endif /* EXCEPTIONHANDLING_H_ */
