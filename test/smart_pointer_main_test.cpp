//============================================================================
// Name        : main_test.cpp
// Author      : Chad Zammar
// Version     :
// Copyright   : © 2020 Chad Zammar
// Description : Simple smart pointer implementation for teaching purposes
//============================================================================

#include <iostream>
#include <cmath>

#include "copiablesmartpointer.hpp"
#include "smartpointer.hpp"

using namespace std;

int main() {
	const float epsilon = 0.001;

	// Test cases
	{
		cout << "test smart pointer with one int element initialized to 11: ";
		SmartPointer<int> sp(11);
		if (sp.getValue() == 11)
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	{
		cout << "test smart pointer with one float element with default value 0: ";
		SmartPointer<float> sp;
			if (sp.getValue() == 0)
				cout << "PASS" << endl;
			else
				cout << "FAIL" << endl;
	}

	{
		cout << "test smart pointer with negative value (exception should be thrown): ";
		try {
			SmartPointer<int> sp(-4);
		}
		catch (const NegativeNumberException& ex) {
			cout << "PASS" << endl;
		}
		catch (...) {
			cout << "FAIL" << endl;
		}
	}

	{
		cout << "test the sum of 2 smart pointers holding each one element: ";
		SmartPointer<float> sp1(5.5f);
		SmartPointer<float> sp2(2.2f);
		SmartPointer<float> sp3 = sp1 + sp2;
		if (fabs(sp3.getValue() - 7.7f) <= epsilon)
			cout << "PASS" << endl;
		else
			cout << "FAIL " << endl;
	}

	{
		cout << "test the sum of 2 smart pointers holding each 3 float elements: ";
		SmartPointer<float> sp1(1.1f, 3);
		sp1.setValue(2.2f, 1);
		sp1.setValue(3.3f, 2);

		SmartPointer<float> sp2(1.1f, 3);
		sp2.setValue(4.4f, 2);

		SmartPointer<float> sp3 = sp1 + sp2;

		if ( (fabs(sp3.getValue(0) - 2.2f) <= epsilon) &&
			 (fabs(sp3.getValue(1) - 3.3f) <= epsilon) &&
			 (fabs(sp3.getValue(2) - 7.7f) <= epsilon) )
			cout << "PASS" << endl;
		else
		{
			cout << "FAIL " << endl;
		}

	}

	{
		cout << "test that copiable smart pointer is making a deep copy: ";
		CopiableSmartPointer<int> sp2;
		{
			CopiableSmartPointer<int> sp1(13, 3);
			sp2 = sp1;
		}
		if ( (sp2.getValue(0)) == 13 &&
			 (sp2.getValue(1)) == 13 &&
			 (sp2.getValue(2)) == 13 )
			cout << "PASS" << endl;
		else
		{
			cout << "FAIL " << endl;
		}

	}

	return 0;
}
