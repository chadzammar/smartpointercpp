/*
 * copyablesmartpointer.hpp
 *
 *  Created on: Feb. 1, 2020
 *  Author: Chad
 */

#ifndef COPIABLESMARTPOINTER_HPP_
#define COPIABLESMARTPOINTER_HPP_

#include "smartpointer.hpp"

template<typename T>
class CopiableSmartPointer : public SmartPointer<T> {
public:
	// Default constructor
	CopiableSmartPointer():SmartPointer<T>() {};
	// Constructor with parameters
	CopiableSmartPointer(T initialValue, unsigned int arraySize = 1):
		SmartPointer<T>(initialValue, arraySize) {};

	// copy constructor
	CopiableSmartPointer(const CopiableSmartPointer& other) {
		this->allocateMemory(other.size);
		for (unsigned int i = 0; i < other.size; i++) {
				this->value[i] = other.value[i];
		}
	}

	// copy assignment operator
	CopiableSmartPointer& operator=(const CopiableSmartPointer& other) {
		// Self-assignment detection
		if (&other == this)
			return *this;

		// release already held resource
		delete[] this->value;
		// allocate new one
		this->allocateMemory(other.size);

		for (unsigned int i = 0; i < other.size; i++) {
				this->value[i] = other.value[i];
		}

		return *this;
	}
};



#endif /* COPIABLESMARTPOINTER_HPP_ */
