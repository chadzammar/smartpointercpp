# SmartPointerCPP

A simple smart pointer implementation for teaching purpose to show a bit of RAII design. In this sample, we will try to tackle the implementation of a “smart pointer” in its simplest form.


SmartPointer is a class template that can allocate one or more elements of a specified built-in type. 
It has basic exception handling mechanism to protect against the following cases:

+	The user is not allowed to enter a negative value.
+	The size of the array should also be positive.
+	Out of memory exceptions should be signaled.

The class also provides a simple operator overloading in order to offer arithmetic operations to be applied on the elements being held by the smart pointer. 

Few test cases are provided under test folder. The test cases don't cover all the scenarios, but are presented as a sample to show you the way.

This class is not thread safe nor it is complete (no reference counting, no copy semantics ...).
The purpose of this class is solely for teaching and presenting an example about how smart pointers work in their most basic forms while touching on subjects like template programming and operator overloading.

Another inherited version is provided in the class CopiableSmartPointer. This version implements a copy constructor and a copy assignment operator to be able to deal explicitly with deep copies.

Author: Chad Zammar (chad <dot> zammar <at> gmail <dot> com)