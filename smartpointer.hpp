/*
 * smartpointer.hpp
 *
 *  Created on: Feb. 1, 2020
 *  Author: Chad
 */

#ifndef SMARTPOINTER_HPP_
#define SMARTPOINTER_HPP_

#include <iostream>
#include "exceptionhandling.h"

using namespace std;

NegativeNumberException 	negative_number_exception;
RangeException 				range_exception;
IllegalArraySizeException	array_size_exception;

template <typename T>
class SmartPointer {
public:
	// Default constructor
	SmartPointer();
	// Constructor with parameters
	SmartPointer(T initialValue, unsigned int arraySize = 1);
	// Destructor
	virtual ~SmartPointer();

	T getValue(unsigned int index = 0);
	void setValue(T newValue, unsigned int index = 0);
	unsigned int getSize() const { return size; }
	void printValues();

	// Friend functions
	template <typename U>
	friend SmartPointer<U> operator+(const SmartPointer<U>& first, const SmartPointer<U>& second);

	template <typename U>
	friend SmartPointer<U> operator-(const SmartPointer<U>& first, const SmartPointer<U>& second);

	template <typename U>
	friend SmartPointer<U> operator*(const SmartPointer<U>& first, const SmartPointer<U>& second);

	// protected methods
protected:
	void allocateMemory(int allocSize = 1);

	// protected members
protected:
	T* value;
	unsigned int size;
};


// Implementation of member methods

// Default constructor
template <typename T>
SmartPointer<T>::SmartPointer() {
	allocateMemory();	// Allocate memory
	value[0] = 0;		// Initialize value to 0
}

// Constructor with parameters
template <typename T>
SmartPointer<T>::SmartPointer(T initialValue, unsigned int arraySize) {
	if (initialValue < 0)	{
		throw negative_number_exception;
	}

	allocateMemory(arraySize);	// Allocate memory

	// Set all the elements in the array to the given initial value
	for (unsigned int i = 0; i < arraySize; i++) {
		value[i] = initialValue;
	}
}

template <typename T>
SmartPointer<T>::~SmartPointer() {
	delete[] value; // release memory
}

template <typename T>
void SmartPointer<T>::setValue(T newValue, unsigned int index) {
	// If given value is not allowed (negative), then throw exception
	if (newValue < 0) {
		throw negative_number_exception;
	}

	// throw exception if index is out of range
	if (index >= size) {
		throw range_exception;
	}

	value[index] = newValue;
}

template <typename T>
T SmartPointer<T>::getValue(unsigned int index)
{
	// if index is out of array size, then throw exception
	if (index >= size)
	{
		throw range_exception;
	}

	return value[index];
}

template <typename T>
void SmartPointer<T>::allocateMemory(int allocSize) {
	if (allocSize < 1) {
		throw array_size_exception; // throw exception if size is less than one
	}

	try	{
		value = new T[allocSize];
		size = allocSize;
	}
	catch (const bad_alloc& e)	{
		throw "Out of memory ...";
	}
}

// Pretty print the values of a smart pointer
template<typename T>
void SmartPointer<T>::printValues() {
	for (unsigned int i = 0; i < size; i++) {
		cout << value[i];
		if (i < size - 1) {
			cout << ", "; // separate values by a comma except for the last element
		}
	}

	cout << endl;
}

// Implementations for friend functions:

// Adding the elements of two arrays
template<typename T>
SmartPointer<T> operator+(const SmartPointer<T>& first, const SmartPointer<T>& second) {
	unsigned int size = first.size > second.size ? first.size : second.size;	// determine max size of arrays
	SmartPointer<T> sp = SmartPointer<T>(0, size);	// create SmartPointer with max size
	for (unsigned int i = 0; i < size; i++)	{		// and fill with sum of elements first + second
		// if one of arrays is shorter, then count next elements as 0
		sp.value[i] = (i < first.size ? first.value[i] : 0) + (i < second.size ? second.value[i] : 0);
	}

	return sp;
}

// Subtracting the elements of two arrays
template<typename T>
SmartPointer<T> operator-(const SmartPointer<T>& first, const SmartPointer<T>& second) {
	unsigned int size = first.size > second.size ? first.size : second.size;	// determine max size of arrays
	SmartPointer<T> sp = SmartPointer<T>(0, size);	// create SmartPointer with max size
	for (unsigned int i = 0; i < size; i++)	{		// and fill with subtraction of elements first - second
		// if one array is shorter, then count next elements as 0
		sp.value[i] = (i < first.size ? first.value[i] : 0) - (i < second.size ? second.value[i] : 0);
	}

	return sp;
}

// Multiplying the elements of two arrays
template<typename T>
SmartPointer<T> operator*(const SmartPointer<T>& first, const SmartPointer<T>& second) {
	unsigned int size = first.size > second.size ? first.size : second.size;	// determine max size of arrays
	SmartPointer<T> sp = SmartPointer<T>(0, size);	// create SmartPointer with max size
	for (unsigned int i = 0; i < size; i++)	{		// and fill with multiplication of elements one on one first X second
		// if one array is shorter, then count next elements as 1
		sp.value[i] = (i < first.size ? first.value[i] : 1) * (i < second.size ? second.value[i] : 1);
	}

	return sp;
}



#endif /* SMARTPOINTER_HPP_ */
